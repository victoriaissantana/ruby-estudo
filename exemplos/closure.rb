contador = lambda {
  acumula = 0
  lambda { acumula = acumula + 1 }
}

c = contador.call()
puts c.call # 1
puts c.call # 2
puts c.call # 3
puts c.call # 4
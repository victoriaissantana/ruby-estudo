#Exemplo de comparação estilo declarativo e estilo imperativo
#imperativo
lista = [1,2,3,4]
i = 0
while i < lista.size
	lista[i] = lista[i] *2
	i = i + 1
end
p lista # [2,4,6,8]


#declarativo
p [1,2,3,4].map{|valor| valor*2} # [2,4,6,8]

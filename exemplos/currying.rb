operacao = ->(func, a, b) { a.send(func, b) }

soma = operacao.curry.(:+)
soma_com_dois = operacao.curry.(:+, 2)
soma_de_dois_e_tres = operacao.curry.(:+, 2, 3)

p soma.(5, 6) # 11
p soma_com_dois.(2) # 4
p soma_de_dois_e_tres # 5
#Exemplo função de alta ordem
numeros_de_letras = ["Henrique", "Melissa", "Jackson", "Leonardo"].map { |x| x.size }

p numeros_de_letras

#O map itera sobre a lista, passando cada elemento da lista como 
#argumento para a expressão lambda, a lógica então é processada, 
#logo, o elemento antigo é substituido pelo retorno da expressão lambda.